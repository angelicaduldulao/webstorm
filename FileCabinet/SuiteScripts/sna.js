/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/file', 'N/runtime'],

function(search, file, runtime) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	if (scriptContext.type == 'print') {
    		var currentscript = runtime.getCurrentScript();
			var starturl = currentscript.getParameter('custscript_url_start');
			var unmarkedradio = currentscript.getParameter('custscript_unmarked_radio_icon');
			var markedradio = currentscript.getParameter('custscript_marked_radio_icon');
			var unmarkedcheck = currentscript.getParameter('custscript_unmarked_check_icon');
			var markedcheck = currentscript.getParameter('custscript_marked_check_icon');
			log.debug('beforeLoad', 'starturl: ' + starturl + ' | unmarkedradio: ' + unmarkedradio + ' | markedradio: ' + markedradio + ' | unmarkedcheck: ' + unmarkedcheck + ' | markedcheck: ' + markedcheck);
			
    		var form = scriptContext.form;
    		form.addField({id: 'custpage_location', type: 'longtext', label: 'location'});
    		form.addField({id: 'custpage_itemimg', type: 'longtext', label: 'itemimg'});
    		form.addField({id: 'custpage_packageimg', type: 'longtext', label: 'packageimg'});
			form.addField({id: 'custpage_customer', type: 'longtext', label: 'customer'});
			form.addField({id: 'custpage_desc', type: 'longtext', label: 'description'});
			form.addField({id: 'custpage_foamfab', type: 'longtext', label: 'foam fab'});	
			form.addField({id: 'custpage_marked_radio', type: 'longtext', label: 'marked radio'});
			form.addField({id: 'custpage_unmarked_radio', type: 'longtext', label: 'unmarked radio'});
			form.addField({id: 'custpage_marked_check', type: 'longtext', label: 'marked check'});
			form.addField({id: 'custpage_unmarked_check', type: 'longtext', label: 'unmarked check'});
			form.addField({id: 'custpage_packaging_itms', type: 'longtext', label: 'packaging items'}); 
			
			var itemimageurl = '';
			var packagingimgurl = '';
			
			var rec = scriptContext.newRecord;
			var item = rec.getValue({fieldId: 'custrecord_sna_part_item'});		
			var packagingimg = rec.getValue({fieldId: 'custrecord_sna_packaging_img'});
			var partsperpack = rec.getValue({fieldId: 'custrecord_sna_parts_per_package'});
			
			// Packaging image
			if (!isEmpty(packagingimg)) {
				packagingimgurl = starturl + file.load(packagingimg).url;		
			}
			rec.setValue({fieldId: 'custpage_packageimg', value: packagingimgurl});
			
			if (!isEmpty(item)) {
				// Item image
				var itemimage = search.lookupFields({type: 'item', id: item, columns: ['storedisplayimage', 'description', 'location', 'custitem_sna_item_ownership.companyname']});
				rec.setValue({fieldId: 'custpage_location', value: itemimage.location[0].text});
				rec.setValue({fieldId: 'custpage_desc', value: itemimage.description});
				
				if (!isEmpty(itemimage['custitem_sna_item_ownership.companyname'])) {
					rec.setValue({fieldId: 'custpage_customer', value: itemimage['custitem_sna_item_ownership.companyname']});				
				}
				
				if (!isEmpty(itemimage.storedisplayimage)) {
					itemimage = itemimage.storedisplayimage[0].value;
					
					if (!isEmpty(itemimage)) {
						itemimageurl = starturl + file.load(itemimage).url;							
					}
				}				
				rec.setValue({fieldId: 'custpage_itemimg', value: itemimageurl});				
				
				// tool search
				/*var filters = [];
				filters.push(search.createFilter({name: 'custrecord_sna_tool_item', operator: 'anyof', values: item}));				
				var columns = [];
				columns.push(search.createColumn({name: 'internalid', sort : search.Sort.DESC}));
				columns.push(search.createColumn({name: 'altname', join: 'custrecord_sna_ownership'}));
				
				var _search = search.create({type: 'customrecordsna_tool', filters: filters, columns: columns});
				_search.run().each(function(result) {  
					rec.setValue({fieldId: 'custpage_customer', value: result.getValue({name:'altname', join: 'custrecord_sna_ownership'})});				
					
					return false; // first result
    			});*/
				
				// member item search
				var memberitems = [];
				
				var memfilters = [];
				memfilters.push(search.createFilter({name: 'internalid', operator: 'anyof', values: item}));	
				memfilters.push(search.createFilter({name: 'custitem_sna_item_category', join: 'memberitem', operator: 'anyof', values: '5'})); // Packaging	
				var memcolumns = [];
				memcolumns.push(search.createColumn({name: 'memberitem'}));
				memcolumns.push(search.createColumn({name: 'memberquantity'}));
				
				var _memsearch = search.create({type: 'item', filters: memfilters, columns: memcolumns});
				_memsearch.run().each(function(result) {  
					var obj = {};
					obj.itm = result.getText({name:'memberitem'});
					obj.qty = Math.round(forceFloat(result.getValue({name:'memberquantity'})) * forceFloat(partsperpack));			
					
					memberitems.push(obj);
					
					return true; 
    			});
				
				if (!isEmpty(memberitems)) {
					rec.setValue({fieldId: 'custpage_packaging_itms', value: JSON.stringify(memberitems)});   
				}
				
				rec.setValue({fieldId: 'custpage_marked_radio', value: escapeXml(markedradio)});
				rec.setValue({fieldId: 'custpage_unmarked_radio', value: escapeXml(unmarkedradio)});
				rec.setValue({fieldId: 'custpage_marked_check', value: escapeXml(markedcheck)});
				rec.setValue({fieldId: 'custpage_unmarked_check', value: escapeXml(unmarkedcheck)});
			}
    	}
    }

    return {
        beforeLoad: beforeLoad
    };
    
});

// UTILITY FUNCTIONS
function isEmpty(stValue) {
	return ((stValue === '' || stValue == null || stValue == undefined) || (stValue.constructor === Array && stValue.length == 0) || (stValue.constructor === Object && (function(v) {
		for ( var k in v)
			return false;
		return true;
	})(stValue)));
}

function forceFloat(stValue) {
	var flValue = parseFloat(stValue);
	if (isNaN(flValue) || (stValue == 'Infinity')) {
		return 0.00;
	}
	return flValue;
}

function escapeXml(unsafe) {
	unsafe = encodeURIComponent(unsafe);
	if (unsafe.indexOf('%E2%80%90') > -1) {
		unsafe = unsafe.replace('%E2%80%90', '-');
	}
	unsafe = decodeURIComponent(unsafe);
	
    return unsafe.replace(/[<>&'"]/g, function (c) {
        switch (c) {
            case '<': return '&lt;';
            case '>': return '&gt;';
            case '&': return '&amp;';
            case '\'': return '&apos;';
            case '"': return '&quot;';
        }
    });
}
